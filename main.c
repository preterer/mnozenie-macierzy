#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#define SIZE 10 // Rozmiar macierzy (macierz z założenia jest kwadratem)

/**
 * Error codes:
 *   -1: File input error
 */

// VARIABLES

const int RUNTIME_IN_SECONDS = 1;		 // ile sekund ma się wykonywać kod liczenia macierzy; potrzebne do obliczenia czasu pojedynczego wykonania
const int MAX_NUMBER_LENGTH = 100;	 // zabezpieczenie przed ogromnymi liczbami, które nie powinny się pojawić
const int NUMBER_BASE = 10;					 // Podstawa dziesiętna przy zapisie, przy odczycie ZAWSZE jest dziesiętna
const int FILE_OPEN_ERROR_CODE = -1; // Zazwyczaj plik nieznaleziony, ewentualnie brak uprawnień
// Ścieżki są relatywne, więc trzeba pamiętać o uruchamianiu programu z poziomu folderu, w którym się znajdują
char RESULTS_PATH[] = "macierz_wynikowa.txt";
char TABLE_A_PATH[] = "macierz_a.txt";
char TABLE_B_PATH[] = "macierz_a.txt";
float table_a[SIZE][SIZE] = {};																// tablica, która przyjmie elementy z pliku macierz_a
float table_b[SIZE][SIZE] = {};																// tablica, która przyjmie elementy z pliku macierz_b
float results[SIZE][SIZE] = {};																// tablica na wyniki mnożenia macierzy a i b
float min_a, min_b, max_a, max_b, sum_a, sum_b, avg_a, avg_b; // Dla bardziej ekektywnego działania kodu - pojedyncza pętla

// UTILS

// DRY
void loop(void (*callback)(int, int))
{
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			callback(i, j);
		}
	}
}

// Wypisuje pojedynczy wpis z danej tablicy
void printEntry(float table[SIZE][SIZE], int i, int j)
{
	printf("[%d][%d]=%.1f ", i, j, table[i][j]);
	if (j == 9)
	{
		printf("\n");
	}
}

void printResults(int i, int j)
{
	printEntry(results, i, j);
}

void printA(int i, int j)
{
	printEntry(table_a, i, j);
}

void printB(int i, int j)
{
	printEntry(table_b, i, j);
}

// FILES

// Po natknięciu się na znak, który jest cyfrą, odczytuje liczbę do końca
const float readNumber(FILE *file, char ch)
{
	char number_string[MAX_NUMBER_LENGTH];
	memset(number_string, 0, MAX_NUMBER_LENGTH * sizeof(char));
	strcat(number_string, &ch);
	while ((ch = fgetc(file)) != EOF && isdigit(ch))
	{
		strcat(number_string, &ch);
	}
	return (float)atoi(number_string);
}

// Wczytuje plik do tablicy
void readFileToTable(char *filePath, float table[SIZE][SIZE])
{
	char ch;
	int i = 0, j = 0;
	FILE *file = fopen(filePath, "r");
	if (file == NULL)
	{
		exit(FILE_OPEN_ERROR_CODE);
	}
	while ((ch = fgetc(file)) != EOF && i < SIZE)
	{
		if (isdigit(ch))
		{
			table[i][j++] = readNumber(file, ch);
		}
		if (j == SIZE)
		{
			i++;
			j = 0;
		}
	}
	fclose(file);
}

// Zapisuje pojedynczy element tablicy do pliku
void printToFile(char *filePath, int i, int j)
{
	char buffer[MAX_NUMBER_LENGTH];
	FILE *file = fopen(filePath, (i == 0 && j == 0) ? "wb" : "ab");
	fputs(itoa(results[i][j], buffer, NUMBER_BASE), file);
	fputs((j == 9) ? "\n" : ", ", file);
	fclose(file);
}

void printResultsToFile(int i, int j)
{
	printToFile(RESULTS_PATH, i, j);
}

// MATH

// Oblicza wartość komórki macierzy wynikowej
void calcResult(int i, int j)
{
	float result = 0.;
	for (int a = 0; a < SIZE; a++)
	{
		result += table_a[i][a] * table_b[a][j];
	}
	results[i][j] = result;
}

float min(float new, float current)
{
	return (new < current) ? new : current;
}

float max(float new, float current)
{
	return (new > current) ? new : current;
}

// Aktualizuje parametry macierzy a i b przy każdym elemencie
void updateParameters(int i, int j)
{
	float val_a = table_a[i][j], val_b = table_b[i][j];
	min_a = min(val_a, min_a);
	max_a = max(val_a, max_a);
	min_b = min(val_b, min_b);
	max_b = max(val_b, max_b);
	sum_a += val_a;
	sum_b += val_b;
}

void calcAvg()
{
	avg_a = sum_a / SIZE;
	avg_b = sum_b / SIZE;
}

// Wylicza parametry macierzy a i b
void calcParameters()
{
	min_a = max_a = table_a[0][0];
	min_b = max_b = table_b[0][0];
	sum_a = sum_b = 0.;
	loop(updateParameters);
	calcAvg();
}

void benchmark(void (*benchmarked)())
{
	time_t start = time(0), end = time(0);
	int loops = 0;
	do
	{
		benchmarked();
		end = time(0);
		loops++;
	} while (end - start < RUNTIME_IN_SECONDS);
	printf("Czas wykonania programu: %d sekund \n", RUNTIME_IN_SECONDS);
	printf("Liczba wykonan: %d \n", loops);
	printf("Czas na pojedyncze wykonanie: %12.10f mikrosekund \n\n", ((double)RUNTIME_IN_SECONDS / (double)loops) * 1000000.0);
}

void run()
{
	loop(calcResult);
	calcParameters();
}

int main()
{
	readFileToTable(TABLE_A_PATH, table_a);
	readFileToTable(TABLE_B_PATH, table_b);

	benchmark(run);
	printf("MIN A: %.1f, MIN B: %1f, MAX A: %.1f, MAX B: %.1f, AVG A: %.1f, AVG B: %.1f \n\n", min_a, min_b, max_a, max_b, avg_a, avg_b);
	loop(printResultsToFile);

	system("pause");
	return 0;
}
